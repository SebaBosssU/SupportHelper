define('two/supporter', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var SupportHelper = {}
    SupportHelper.version = '__supporter_version'
    SupportHelper.init = function () {
        Locale.create('supporter', __supporter_locale, 'pl')

        SupportHelper.initialized = true
    }
    SupportHelper.run = function () {
        if (!SupportHelper.interfaceInitialized) {
            throw new Error('SupportHelper interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return SupportHelper
})