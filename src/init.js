require([
    'two/ready',
    'two/supporter',
    'two/supporter/ui'
], function (
    ready,
    SupportHelper
) {
    if (SupportHelper.initialized) {
        return false
    }

    ready(function () {
        SupportHelper.init()
        SupportHelper.interface()
        SupportHelper.run()
    })
})
