define('two/supporter/ui', ['two/supporter', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(SupportHelper, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var bindEvents = function() {
    }
    var SupporterInterface = function() {
        ui = new Interface('SupportHelper', {
            activeTab: 'supporter',
            template: '__supporter_html_window',
            css: '__supporter_css_style',
            replaces: {
                locale: Locale,
                version: SupportHelper.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('supporter', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        bindEvents()
        SupportHelper.interfaceInitialized = true
        return ui
    }
    SupportHelper.interface = function() {
        SupportHelper.interface = SupporterInterface()
    }
})